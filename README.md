# README #

### What is this repository for? ###

* This repo is a desktop application to upload and download analysis on the BDCMS website. It is an academic project.
* Version 1

### How do I get set up? ###

* Run ```npm install``` to download all dependencies.

* Run ```npm start to start the application``` to start the desktop application.
* Run ```npm run-script package``` to bundle the application for the windows platform.